package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Course;

@Service
public class CourseDao {

	@Autowired
	CourseRepository CourseRepository;

	public List<Course> getCourses() {
		return CourseRepository.findAll();
	}

	public Course getCourseById(int CourseId) {
		return CourseRepository.findById(CourseId).orElse(null);
	}

	public Course getCourseByName(String CourseName) {
		return CourseRepository.findByName(CourseName);
	}

	public Course addCourse(Course course) {
		return CourseRepository.save(course);
	}

	public Course updateCourse(Course course) {
		return CourseRepository.save(course);
	}

	public void deleteCourseById(int  CourseId) {
		 CourseRepository.deleteById( CourseId);
	}
	
	
}






