//package com.ts;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.dao.StudentDao;
//import com.model.Student;
//
//@RestController
//public class StudentController {
//	
//	@Autowired
//	StudentDao studentDao;
//	
//	@GetMapping("getStudents")
//	public List<Student> getStudents() {
//		return studentDao.getStudents();
//	}
//	
//	@GetMapping("getStudentById/{stuId}")
//	public Student getStudentById(@PathVariable int stuId) {
//		return studentDao.getStuById(stuId);
//	}
//	
//	@GetMapping("getStudentByName/{stuName}")
//	public Student getStudentByName(@PathVariable String stuName) {
//		return studentDao.getStuByName(stuName);
//	}
//	
//	@GetMapping("studentLogin/{emailId}/{password}")
//	public Student studentLogin(@PathVariable String emailId, @PathVariable String password) {
//		return studentDao.studentLogin(emailId, password);
//	}
//	
//	@PostMapping("addStudent")
//	public Student addStudent(@RequestBody Student student) {
//		return studentDao.addStudent(student);
//	}
//	
//	@PutMapping("updateStudent")
//	public Student updateStudent(@RequestBody Student student) {
//		return studentDao.updateStudent(student);
//	}
//	
//	@DeleteMapping("deleteStudentById/{stuId}")
//	public String deleteStudentById(@PathVariable int stuId) {
//	    studentDao.deleteStudentById(stuId);
//	    return "Student with stuId: " + stuId + " Deleted Successfully!!!";
//	}
//	
//}
//


package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {

    @Autowired
    StudentDao studentDao;

    @GetMapping("getStudents")
    public List<Student> getStudents() {
        return studentDao.getStudents();
    }

    @GetMapping("getStudentById/{stuId}")
    public Student getStudentById(@PathVariable int stuId) {
        return studentDao.getStuById(stuId);
    }

    @GetMapping("getStudentByName/{stuName}")
    public Student getStudentByName(@PathVariable String stuName) {
        return studentDao.getStuByName(stuName);
    }

    @GetMapping("studentLogin/{emailId}/{password}")
    public Student studentLogin(@PathVariable String emailId, @PathVariable String password) {
        return studentDao.studentLogin(emailId, password);
    }

    @PostMapping("addStudent")
    public Student addStudent(@RequestBody Student student) {
        return studentDao.addStudent(student);
    }

    @PutMapping("updateStudent")
    public Student updateStudent(@RequestBody Student student) {
        return studentDao.updateStudent(student);
    }

    @DeleteMapping("deleteStudentById/{stuId}")
    public String deleteStudentById(@PathVariable int stuId) {
        studentDao.deleteStudentById(stuId);
        return "Student with stuId: " + stuId + " Deleted Successfully!!!";
    }
}

