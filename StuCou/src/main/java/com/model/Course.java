package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Course {
	
	@Id
	private int courseId;			
	private String courseName;	
	private int duration;
	
	//Implementing Mapping Between Department and Employee
	@JsonIgnore	//UnComment this later on
	@OneToMany(mappedBy="course")
	List<Student> stuList = new ArrayList<Student>();
	
	public Course() {
	}

	public Course(int courseId, String courseName, int duration) {
		this.courseId = courseId;
		this.courseName = courseName;
		this.duration = duration;
	}
	
	//Generating Getter for empList Variable
	public List<Student> getStuList() {
		return stuList;
	}

	//Generating Setter for empList Variable
	public void setStuList(List<Student> stuList) {
		this.stuList = stuList;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}





 
