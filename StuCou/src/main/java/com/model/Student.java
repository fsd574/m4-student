package com.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Entity
public class Student {

	@Id@GeneratedValue
	private int stuId;
	private String stuName;
	private double fee;
	private String gender;
	private Date doj;
	private String country;
	private String emailId;
	private String password;
	
	//Implementing Mapping Between Employee and Department
	@ManyToOne
	@JoinColumn(name="courseId")
	Course course;
	
	public Student() {
	}
	
	//Parameterized Constructor without empId
	public Student(String stuName, double fee, String gender, Date doj, String country, String emailId,
			String password) {
		this.stuName = stuName;
		this.fee = fee;
		this.gender = gender;
		this.doj = doj;
		this.country = country;
		this.emailId = emailId;
		this.password = password;
	}

	public Student(int stuId, String stuName, double fee, String gender, Date doj, String country, String emailId,
			String password) {
		this.stuId = stuId;
		this.stuName = stuName;
		this.fee = fee;
		this.gender = gender;
		this.doj = doj;
		this.country = country;
		this.emailId = emailId;
		this.password = password;
	}
		
	//Generating Getter for department Variable
	public Course getCourse() {
		return course;
	}
	
	//Generating Setter for department Variable
	public void setCourse(Course course) {
		this.course = course;
	}

	public int getStuId() {
		return stuId;
	}

	public void setStuId(int stuId) {
		this.stuId = stuId;
	}

	public String getStuName() {
		return stuName;
	}

	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDoj() {
		return doj;
	}

	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
