package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Course {
	
	@Id@GeneratedValue
	private int courseId;
	
	@Column(name="cname")
	private String cName;
	private double cprice;
	
	public Course() {
		super();
	}

	public Course(int courseId, String cName, double cprice) {
		super();
		this.courseId = courseId;
		this.cName = cName;
		this.cprice = cprice;
	}

	public int getcourseId() {
		return courseId;
	}
	public void setcourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}

	public double getcPrice() {
		return cprice;
	}
	public void setcPrice(double cprice) {
		this.cprice = cprice;
	}

	@Override
	public String toString() {
		return "Student [courseId=" + courseId + ", cName=" + cName + ", cprice=" + cprice + "]";
	}	
}


