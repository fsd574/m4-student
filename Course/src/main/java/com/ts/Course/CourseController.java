package com.ts.Course;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.dao.CourseDao;
import com.model.Course;

import java.util.List;

@RestController
public class CourseController {

    @Autowired
    CourseDao courseDao;

    @GetMapping("getAllCourses")
    public List<Course> getAllCourses() {
        return courseDao.getAllCourses();
    }

    @GetMapping("getCourseById/{courseId}")
    public Course getCourseById(@PathVariable("courseId") int courseId) {
        return courseDao.getCourseById(courseId);
    }

    @GetMapping("getCourseByName/{courseName}")
    public List<Course> getCourseByName(@PathVariable("courseName") String courseName) {
        return courseDao.getCourseByName(courseName);
    }

    @PostMapping("addCourse")
    public Course addCourse(@RequestBody Course course) {
        return courseDao.addCourse(course);
    }

//    @PutMapping("updateCourse")
//    public Course updateCourse(@RequestBody Course course) {
//        return courseDao.updateCourse(course);
//    }

    @PutMapping("updateCourseById/{courseId}")
    public Course updateCourseById(@PathVariable("courseId") int courseId, @RequestBody Course updatedCourse) {
        return courseDao.updateCourseById(courseId, updatedCourse);
    }

//    @PutMapping("updateCourseByName/{courseName}")
//    public Course updateCourseByName(@PathVariable("courseName") String courseName, @RequestBody Course updatedCourse) {
//        return courseDao.updateCourseByName(courseName, updatedCourse);
//    }

    @DeleteMapping("deleteCourseById/{id}")
    public String deleteCourseById(@PathVariable("id") int courseId) {
        courseDao.deleteCourseById(courseId);
        return "Course with CourseId: " + courseId + ", Deleted Successfully";
    }
}
